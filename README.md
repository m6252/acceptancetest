# Todo App Acceptance Test


## Libraries & Frameworks:
* [Taiko](https://taiko.dev/) for the Acceptance test
* [Gauge](https://docs.gauge.org/) for the Acceptance test

## Download & Run
- Run `npm install` for package installations.
- Run `npm run test` for acceptance tests.
