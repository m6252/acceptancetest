"use strict";
const {
    write,
    waitFor,
    click,
    into,
    listItem,
    text,
    
} = require('taiko');
const assert = require("assert");



step("Given Empty ToDo list", async ()=>{
    assert.ok(!await listItem({id:'todoList-0'}).exists());
})

step("When I write <item> to <tdtextbox> and click to <addbutton>",async (item,tdtextbox,addbutton)=>{
 await write(item, into(tdtextbox))
 await waitFor(2000)
 await click(addbutton)
 await waitFor(2000)
})
step("Then I should see <item> item in ToDo list", async (item)=>{
    await waitFor(2000)
   
    await text(item).exists();
})