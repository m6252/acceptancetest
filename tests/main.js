/* globals gauge*/
"use strict";

const {
    openBrowser,
    closeBrowser,
    goto,
    waitFor
   
} = require('taiko');

const {ENV} = process.env;
const envirovment = ENV === "LOCAL" ? "http://localhost:8080" : "http://34.78.1.142/" ;

beforeSuite(async () => {
    await openBrowser({
		args: ['--no-sandbox', '--disable-setuid-sandbox'],
	});


   
    await goto(envirovment)
    await waitFor(5000)
});

afterSuite(async () => {
    await closeBrowser();
});

